// Fill out your copyright notice in the Description page of Project Settings.


#include "RobotEnemy.h"

// Sets default values
ARobotEnemy::ARobotEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	boxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));

	boxCollider->SetupAttachment(RootComponent);
	boxCollider->SetNotifyRigidBodyCollision(true);
}

// Called when the game starts or when spawned
void ARobotEnemy::BeginPlay()
{
	Super::BeginPlay();

	rotating = true;
	SetActorLocation(waypointLocations[0]->GetActorLocation());
	currentTarget = 1;
}

// Called every frame
void ARobotEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	AActor* player = GetWorld()->GetFirstPlayerController()->GetPawn();

	TArray<AActor*> result;
	boxCollider->GetOverlappingActors(result);

	if (result.Contains(player)) {
		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	}

	if(rotating)
	{
		FVector targetPos = waypointLocations[currentTarget]->GetActorLocation();

		FRotator targetRot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), targetPos);

		FRotator rotateVector = targetRot - GetActorRotation();

		float rotateAmount = rotateSpeed * DeltaTime;

		if (FMath::Abs(FMath::Abs(rotateVector.Yaw) - FMath::Abs(rotateAmount)) <= FMath::Abs(rotateAmount))
		{

			SetActorRotation(targetRot);
			rotating = false;
		}
		else {

			SetActorRotation(GetActorRotation() + FRotator(0, rotateAmount, 0));
		}
	}
	else
	{
		FVector targetPos = waypointLocations[currentTarget]->GetActorLocation();

		FVector moveVector = targetPos - GetActorLocation();
		moveVector.Normalize();

		float moveDist = speed * DeltaTime;
		moveVector *= moveDist;

		if (FVector::Dist(GetActorLocation(), targetPos) <= moveDist) {
			SetActorLocation(targetPos);
			currentTarget++;

			if (currentTarget >= waypointLocations.Num()) {
				currentTarget = 0;
			}

			rotating = true;
		}
		else {
			SetActorLocation(GetActorLocation() + moveVector);
		}
	}
}
