 // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/Array.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/BoxComponent.h"
#include "Assignment2Character.h"
#include "Kismet/GameplayStatics.h"
#include "RobotEnemy.generated.h"

UCLASS()
class ASSIGNMENT2_API ARobotEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARobotEnemy();

	UPROPERTY(EditAnywhere) UBoxComponent* boxCollider;

	UPROPERTY(EditAnywhere) TArray<AActor*> waypointLocations;
	int currentTarget = 0;
	UPROPERTY(EditAnywhere) float speed;
	UPROPERTY(EditAnywhere) float rotateSpeed;

	bool rotating = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
